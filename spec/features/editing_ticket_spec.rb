require 'rails_helper'

feature "editing tickets" do
  let!(:project) { create(:project) }
  let!(:ticket) { create(:ticket, project: project) }
  before do 
    visit '/'
    first("body table tbody tr .show").click_link project.title 
    first("#tickets li").click_link ticket.title
    click_link "Edit Ticket" 
  end 
  scenario "edit ticket successfully" do 
    fill_in "Title", with: "Gem bug"
    click_button "Update Ticket"
  end 
end 