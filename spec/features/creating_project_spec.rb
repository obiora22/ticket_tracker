require 'rails_helper'

feature "creating project" do 
  before do 
    visit '/'
    click_link 'New Project'
  end 
  scenario "can create a project" do 
    fill_in "Title", with: "Project Ruby"
    fill_in "Description", with: 'A ruby project'
    click_button 'Create Project'
    expect(page).to have_content("Project has been created.")
  end 
  
  scenario 'cannot create a project without a name' do 
    fill_in 'Title', with: ""
    fill_in 'Description', with: 'A ruby project'
    click_button 'Create Project'
    expect(page).to have_content("Title can't be blank")
  end 
end 