require 'rails_helper'

feature 'delete project' do
  let!(:project){ create(:project)}
  scenario 'delete project successfully' do 
    visit '/'
    first("body table tbody tr .three").click_link "Delete"
    expect(page).to have_content("Project has been destroyed!")
  end 
  
  
    
end 