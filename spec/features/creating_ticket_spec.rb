require 'rails_helper'


feature "create a ticket for a project" do 
  let!(:project){ create(:project) }
  before do 
    visit '/'
    first("body table tbody tr .show").click_link project.title
    expect(page.current_url).to eq project_url(project)
    click_link "Create Ticket"
  end 
    scenario 'create ticket successfully' do 
    fill_in "Title", with: 'Gem bug'
    fill_in "Description", with: "Can't access server."
    click_button "Create Ticket"
    expect(page).to have_content("Ticket has been created.")
  end 
  
  scenario "create invalid ticket if title is empty" do 
    fill_in "Title", with: ""
    fill_in "Description", with: "Can't access server"
    click_button "Create Ticket" 
    expect(page).to have_content("Title can't be blank")
  end 
  
  scenario "create invalid ticket with decription of minimum length < 10" do 
    fill_in "Title", with: "Gem bug"
    fill_in "Description", with: "less"
    click_button "Create Ticket"
    expect(page).to have_content("Description is too short (minimum is 10 characters)")
  end 
end 