require 'rails_helper'

feature 'edit project' do 
  let!(:project) { create(:project)}
  before do 
    visit '/'
    first("body table tbody tr .edit").click_link "Edit"
  end 
  scenario 'edit link is available for a project' do 
    expect(page.current_url).to eq(edit_project_url(project))
  end 
  
  scenario "successfully update a project" do 
    fill_in "Title", with: "Gem bug"
    click_button "Edit Project"
    expect(page).to have_content("Project has been updated");
  end 
  
  scenario "unsuccessfully update a project" do 
    fill_in "Title", with: ""
    click_link "Edit Project"
    expect(page).to_not have_content("Project has been updated");
  end 
end 