require 'rails_helper'

feature "viewing projects" do 
  let!(:project){ create(:project) } 
  scenario 'list all projects' do 
    visit '/'
    expect(page).to have_content('Projects')
    first("body table tbody tr td").click_link project.title 
    expect(page.current_url).to eq(project_url(project))
  end 
end 