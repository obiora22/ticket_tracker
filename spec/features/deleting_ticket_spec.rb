require "rails_helper"
feature "delete ticket" do
  let!(:project){ create(:project) }
  let!(:ticket){ create(:ticket, project: project) } 
  before do 
    visit '/'
    first("body table tbody tr .show").click_link project.title 
    first("#tickets li").click_link ticket.title
  end 
  scenario "successfully delete ticket" do
    click_link "Delete Ticket" 
    expect(page).to have_content("Ticket has been deleted.")
    expect(page.current_url).to eq project_url(project)
  end 
end   