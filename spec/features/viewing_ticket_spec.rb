require 'rails_helper'

feature "view a project ticket" do 
  let!(:projectA){ create(:project, title: 'Project A')}
  let!(:projectB){ create(:project, title: 'Project B')}
  let!(:ticketA){ create(:ticket, title: 'Ticket A', description: "Ticket for A", project: projectA) }
  let!(:ticketB){ create(:ticket, title: 'Ticket B', description: "Ticket for B", project: projectB) }
  scenario "viewing ticket for a given project" do 
    visit '/'
    first("body table tbody tr .show").click_link projectA.title 
    expect(page).to have_content("Ticket A")
    expect(page).to_not have_content("Ticket B")
  end 
end   