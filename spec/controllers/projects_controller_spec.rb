require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let!(:project) { create(:project) } 
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "displays error message for missing resource" do 
      get :show, id: 'here'
      expect(response).to redirect_to projects_url 
      message = "The project you're looking for could not be found."
      expect(flash[:alert]).to eq(message)
    end 
    it "returns http success" do
      get :show, id: 1 
      expect(response).to have_http_status(:success)
    end
  end

end
