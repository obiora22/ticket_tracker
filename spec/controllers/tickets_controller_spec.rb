require 'rails_helper'

RSpec.describe TicketsController, type: :controller do
  let!(:project){ create(:project) }
  let!(:ticket){ create(:ticket, project: project) } 
  describe "GET #new" do
    it "returns http success" do
      get :new, project_id: project.id 
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #edit" do
    it "returns http success" do
      get :edit, id: ticket.id, project_id: project.id 
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      get :show, id: ticket.id, project_id: project.id 
      expect(response).to have_http_status(:success)
    end
  end

end
