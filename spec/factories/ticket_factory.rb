FactoryGirl.define do
  factory :ticket do
    title "Test Ticket"
    description "A ticket for testing"
    project nil
  end

end
