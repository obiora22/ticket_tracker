require 'rails_helper'

RSpec.describe Project, type: :model do
  
  context 'project without title' do 
    let(:project){ build(:project, title: "")} 
    it 'is invalid' do 
      expect(project).to_not be_valid   
    end 
  end 
end
