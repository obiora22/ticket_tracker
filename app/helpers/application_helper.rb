module ApplicationHelper
  def page_title(title) 
    default = 'Ticket Tracker'
    if title.empty?
      default
    else 
      default + " | " + title 
    end 
  end 
end
