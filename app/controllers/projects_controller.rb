class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  def index
  end

  def show
   
  end
  
  def new 
    @project = Project.new 
  end 
  
  def index 
    @projects = Project.all 
  end 
  
  def create
    @project = Project.new(project_params)
    if @project.save 
      redirect_to project_path(@project)
      flash[:notice] = "Project has been created."
    else 
      render "new"
    end 
  end 
  
  def edit
    
  end 
  
  def update 
    if @project.update(project_params) 
      flash[:notice] = "Project has been updated."
      redirect_to @project
    else 
      flash[:alert] = "Project has not been updated."
      render :edit 
    end 
  end 
  
  def destroy 
    if @project.destroy 
      flash[:notice] = "Project has been destroyed!"
      redirect_to projects_url 
    else 
      flash[:alert] = "Oops, something went wrong!"
    end
  end 
  private 
  
  def project_params
    params.require(:project).permit(:title, :description)
  end 
  
  def set_project 
    @project = Project.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The project you're looking for could not be found."
    redirect_to projects_url 
  end 
end
