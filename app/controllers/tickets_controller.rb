class TicketsController < ApplicationController
  before_action :set_project
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
   
  def new
    @ticket = @project.tickets.build 
  end
  
  def create 
    @ticket = @project.tickets.build(ticket_params)
    if @ticket.save 
      flash[:notice] = "Ticket has been created."
      redirect_to project_ticket_path(@project,@ticket)
    else
      flash[:alert] = "Ticket wasn't created!"
      render :new 
    end 
  end 
  
  def edit
  end
  
  def update 
    if  @ticket.update(ticket_params)
      flash[:notice] = "Ticket has been updated."
      redirect_to project_ticket_path(@project,@ticket)
    else 
      flash[:alert] = "Tickket has not been updated."
      render :edit 
    end 
  end 
  
  def show
    
  end
  
  
  def destroy 
    if @ticket.destroy
      flash[:notice] = "Ticket has been deleted."
      redirect_to @project 
    else
      flash[:alert]= "Ticket has not been deleted."
    end 
  end 
  
  private 
  
  def set_ticket 
    @ticket = @project.tickets.find(params[:id])
  end 
  
  def set_project 
    @project = Project.find(params[:project_id])
  end 
  def ticket_params
    params.require(:ticket).permit(:title,:description)
  end 
end
