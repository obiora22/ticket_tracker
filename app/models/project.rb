class Project < ActiveRecord::Base
  validates :title, presence: true 
  validates :title, length: {minimum: 5}
 
  has_many :tickets, dependent: :delete_all 
end
